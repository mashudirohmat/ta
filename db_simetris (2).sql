-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2020 at 07:10 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_simetris`
--

-- --------------------------------------------------------

--
-- Table structure for table `def_jabatan_iki`
--

CREATE TABLE `def_jabatan_iki` (
  `DJI_KODE` int(11) NOT NULL,
  `MST_IKI_KODE` int(11) DEFAULT NULL,
  `REF_JAB_KODE` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `def_jabatan_iki`
--

INSERT INTO `def_jabatan_iki` (`DJI_KODE`, `MST_IKI_KODE`, `REF_JAB_KODE`) VALUES
(45, 40, 'DAITI01'),
(47, 42, 'DAITI01'),
(48, 43, 'DAITI01'),
(49, 44, 'DAITI01'),
(50, 45, 'DAITI01'),
(51, 46, 'DAITI01'),
(52, 47, 'DAITI01'),
(53, 48, 'DAITI01'),
(54, 49, 'PRITI01'),
(55, 50, 'PRITI01');

-- --------------------------------------------------------

--
-- Table structure for table `def_jabatan_logbook`
--

CREATE TABLE `def_jabatan_logbook` (
  `DJL_KODE` int(11) NOT NULL,
  `MKL_KODE` int(11) DEFAULT NULL,
  `REF_JAB_KODE` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `def_jabatan_logbook`
--

INSERT INTO `def_jabatan_logbook` (`DJL_KODE`, `MKL_KODE`, `REF_JAB_KODE`) VALUES
(1, 2, 'KPITI01'),
(3, 2, 'KPITI01'),
(4, 4, 'DAITI01'),
(5, 5, 'DAITI01'),
(6, 3, NULL),
(7, NULL, 'DAITI01'),
(10, 15, 'DAITI01'),
(11, 16, 'PRITI01'),
(13, 18, 'DAITI01');

-- --------------------------------------------------------

--
-- Table structure for table `def_unit_iki`
--

CREATE TABLE `def_unit_iki` (
  `DUI_KODE` int(11) NOT NULL,
  `MST_IKI_KODE` int(11) DEFAULT NULL,
  `MST_UNIT_MSU_KODE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `def_unit_iki`
--

INSERT INTO `def_unit_iki` (`DUI_KODE`, `MST_IKI_KODE`, `MST_UNIT_MSU_KODE`) VALUES
(37, 40, 1),
(39, 42, 1),
(40, 43, 1),
(41, 44, 1),
(42, 45, 1),
(43, 46, 1),
(44, 47, 1),
(45, 48, 1),
(46, 49, 1),
(47, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `def_unit_logbook`
--

CREATE TABLE `def_unit_logbook` (
  `DUL_KODE` int(11) NOT NULL,
  `MKL_KODE` int(11) DEFAULT NULL,
  `MST_UNIT_MSU_KODE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `def_unit_logbook`
--

INSERT INTO `def_unit_logbook` (`DUL_KODE`, `MKL_KODE`, `MST_UNIT_MSU_KODE`) VALUES
(1, 2, 1),
(2, 2, 1),
(3, 3, NULL),
(4, 4, 1),
(5, 5, 2),
(6, NULL, 1),
(9, 15, 1),
(10, 16, 1),
(12, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_iki`
--

CREATE TABLE `mst_iki` (
  `MST_IKI_KODE` int(11) NOT NULL,
  `MST_IKI_INDIKATOR` text NOT NULL,
  `MST_IKI_DEFINISI` varchar(200) NOT NULL,
  `MST_IKI_TARGET` int(3) NOT NULL,
  `MST_IKI_BOBOT` int(3) NOT NULL,
  `MST_IKI_KATEGORI` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_iki`
--

INSERT INTO `mst_iki` (`MST_IKI_KODE`, `MST_IKI_INDIKATOR`, `MST_IKI_DEFINISI`, `MST_IKI_TARGET`, `MST_IKI_BOBOT`, `MST_IKI_KATEGORI`) VALUES
(40, 'Kualitas1', 'Kualitas1', 10, 5, 'kualitas'),
(42, 'Perilaku1', 'Perilaku1', 100, 15, 'perilaku'),
(43, 'Prilaku2', 'Prilaku2', 10, 5, 'perilaku'),
(44, 'Perilaku3', 'Perilaku3', 10, 5, 'perilaku'),
(45, 'Perilaku4', 'Perilaku4', 10, 5, 'perilaku'),
(46, 'Logbook Data Analys', 'Logbook Data Analys', 100, 50, 'kuantitas'),
(47, 'Kualitas2', 'Kualitas2', 5, 10, 'kualitas'),
(48, 'Kualitas3', 'Kualitas3', 24, 5, 'kualitas'),
(49, 'Kulaitas1', 'Kulaitas1', 5, 5, 'kualitas'),
(50, 'Logbook Programmer', 'Logbook Programmer', 100, 50, 'kuantitas');

-- --------------------------------------------------------

--
-- Table structure for table `mst_kegiatan_logbook`
--

CREATE TABLE `mst_kegiatan_logbook` (
  `MKL_KODE` int(11) NOT NULL,
  `MKL_NAMA` varchar(200) NOT NULL,
  `MKL_KETERANGAN` varchar(200) NOT NULL,
  `MKL_SCORE` int(3) NOT NULL,
  `MKL_ISAKTIF` int(1) NOT NULL DEFAULT '1',
  `MST_UNIT_MSU_KODE` varchar(50) DEFAULT NULL,
  `REF_JN_JB_FNG_RJJABF_KOD` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_kegiatan_logbook`
--

INSERT INTO `mst_kegiatan_logbook` (`MKL_KODE`, `MKL_NAMA`, `MKL_KETERANGAN`, `MKL_SCORE`, `MKL_ISAKTIF`, `MST_UNIT_MSU_KODE`, `REF_JN_JB_FNG_RJJABF_KOD`) VALUES
(2, 'Rapat koordinasi', 'Melakukan rapat koordinasi setiap bulan sekali', 4, 1, '1', 'KPITI01'),
(3, 'Melakukan penelitian', 'Penelitian dilakukan untuk kemajuan instansi', 4, 1, NULL, NULL),
(4, 'Kegiatan 3', 'Kegiatan 3', 3, 1, '1', 'DAITI01'),
(5, 'Kegiatan 4', 'Kegiatan 4', 2, 1, '2', 'DAITI01'),
(15, 'test1', 'test1', 2, 1, '1', 'DAITI01'),
(16, 'test2', 'test2', 2, 1, '1', 'PRITI01'),
(18, 'test666', 'test6', 4, 1, '1', 'DAITI01');

-- --------------------------------------------------------

--
-- Table structure for table `mst_pegawai`
--

CREATE TABLE `mst_pegawai` (
  `MPG_KODE` int(11) NOT NULL,
  `MPG_HANDKEY` varchar(20) NOT NULL,
  `MPG_NAMA` varchar(100) NOT NULL,
  `MPG_NIP` varchar(20) NOT NULL,
  `MPG_ALAMAT` varchar(200) NOT NULL,
  `MPG_JK` varchar(1) NOT NULL,
  `MPG_NO_TELP` varchar(13) NOT NULL,
  `MPG_EMAIL` varchar(50) NOT NULL,
  `MPG_ISAKTIF` int(1) NOT NULL,
  `REF_AGAMA_RAG_KODE` int(11) NOT NULL,
  `MPG_IS_VERIF` int(1) NOT NULL,
  `MPG_TMPT_LAHIR` varchar(100) NOT NULL,
  `MPG_TGL_LAHIR` datetime NOT NULL,
  `MPG_FOTO` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_pegawai`
--

INSERT INTO `mst_pegawai` (`MPG_KODE`, `MPG_HANDKEY`, `MPG_NAMA`, `MPG_NIP`, `MPG_ALAMAT`, `MPG_JK`, `MPG_NO_TELP`, `MPG_EMAIL`, `MPG_ISAKTIF`, `REF_AGAMA_RAG_KODE`, `MPG_IS_VERIF`, `MPG_TMPT_LAHIR`, `MPG_TGL_LAHIR`, `MPG_FOTO`) VALUES
(1, 'MASHUDI', 'MASHUDI ROHMAT', '01201912345', 'Pudak Rt 02 Rw 02 Sanggrong,Jatiroto,Wonogiri,Jawa Tengah', 'L', '085642084961', 'mashudi@gmail.com', 1, 1, 1, 'WONOGIRI', '1994-11-08 00:00:00', '1578360262306.jpg'),
(2, 'FATMA', 'SITI FATMAWATI', '01201912344', 'Boyolali', 'P', '085123456789', 'fatma@gmail.com', 1, 1, 1, 'Kalimantan', '1999-11-29 00:00:00', 'fatma.jpg'),
(3, 'NOVA', 'NOVA ARIF', '123456', 'Jogja', 'L', '123456', 'nova@gmail.com', 1, 1, 1, 'Jogja', '2019-12-02 00:00:00', '1578360262306.jpg'),
(4, ' h06', ' hjghfgf', '022020010646', 'mmm', '', 'hjhj', 'fbnvb@ggg.com', 1, 3, 1, 'Jogja', '2020-01-05 20:09:52', '15782664665741.jpg'),
(5, 'HAR07', 'HARRY KANE', '01202001076', 'Tottenham', 'L', '1234', 'hary@gmail.com', 1, 1, 1, 'Tottenham', '2020-01-05 20:09:52', '1578360262306.jpg'),
(6, 'CAM07', 'CAMILA CABELLO', '01202001077', 'Brazil', 'P', '123456789', 'camila@gmail.com', 1, 1, 1, 'Brazil', '2020-01-05 20:09:52', '1578360398164.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mst_unit`
--

CREATE TABLE `mst_unit` (
  `MSU_KODE` int(11) NOT NULL,
  `MSU_NAMA` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_unit`
--

INSERT INTO `mst_unit` (`MSU_KODE`, `MSU_NAMA`) VALUES
(1, 'INSTALASI TEKNOLOGI INFORMASI'),
(2, 'INSTALASI KESEHATAN ANAK');

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `ID_PENILAIAN` int(11) NOT NULL,
  `MPG_KODE` int(11) NOT NULL,
  `TGL_PENILAIAN` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IKI_KODE` int(11) NOT NULL,
  `SCORE` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`ID_PENILAIAN`, `MPG_KODE`, `TGL_PENILAIAN`, `IKI_KODE`, `SCORE`) VALUES
(36, 2, '2020-03-14 06:04:04', 42, 90),
(37, 2, '2020-03-14 06:04:04', 43, 9),
(38, 2, '2020-03-14 06:04:04', 44, 9),
(39, 2, '2020-03-14 06:04:04', 45, 9),
(40, 2, '2020-03-14 06:04:05', 40, 9),
(41, 2, '2020-03-14 06:04:05', 47, 5),
(42, 2, '2020-03-14 06:04:05', 48, 20),
(43, 2, '2020-02-29 05:17:58', 42, 100),
(44, 2, '2020-02-29 05:17:59', 43, 10),
(45, 2, '2020-02-29 05:17:59', 44, 10),
(46, 2, '2020-02-29 05:17:59', 45, 10),
(47, 2, '2020-02-29 05:17:59', 40, 10),
(48, 2, '2020-02-29 05:17:59', 47, 5),
(49, 2, '2020-02-29 05:17:59', 48, 24),
(50, 2, '2020-01-29 06:51:15', 42, 99),
(51, 2, '2020-01-29 06:51:15', 43, 9),
(52, 2, '2020-01-29 06:51:15', 44, 9),
(53, 2, '2020-01-29 06:51:15', 45, 9),
(54, 2, '2020-01-29 06:51:15', 40, 9),
(55, 2, '2020-01-29 06:51:15', 47, 5),
(56, 2, '2020-01-29 06:51:15', 48, 24),
(57, 2, '2019-12-29 06:52:33', 42, 100),
(58, 2, '2019-12-29 06:52:33', 43, 9),
(59, 2, '2019-12-29 06:52:33', 44, 9),
(60, 2, '2019-12-29 06:52:33', 45, 9),
(61, 2, '2019-12-29 06:52:33', 40, 10),
(62, 2, '2019-12-29 06:52:33', 47, 5),
(63, 2, '2019-12-29 06:52:33', 48, 24),
(64, 2, '2020-04-01 04:35:21', 42, 100),
(65, 2, '2020-04-01 04:35:21', 43, 10),
(66, 2, '2020-04-01 04:35:21', 44, 10),
(67, 2, '2020-04-01 04:35:21', 45, 10),
(68, 2, '2020-04-01 04:35:21', 40, 10),
(69, 2, '2020-04-01 04:35:21', 47, 5),
(70, 2, '2020-04-01 04:35:21', 48, 23);

-- --------------------------------------------------------

--
-- Table structure for table `ref_agama`
--

CREATE TABLE `ref_agama` (
  `RAG_KODE` int(11) NOT NULL,
  `REF_AGAMA_NAMA` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_agama`
--

INSERT INTO `ref_agama` (`RAG_KODE`, `REF_AGAMA_NAMA`) VALUES
(1, 'Islam'),
(2, 'Kristen'),
(3, 'Hindu'),
(4, 'Budha'),
(5, 'Katholik'),
(6, 'Protestan');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jb_fungsional`
--

CREATE TABLE `ref_jb_fungsional` (
  `REF_JB_FN_KODE` varchar(11) NOT NULL,
  `REF_JB_FN_NAMA` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jb_fungsional`
--

INSERT INTO `ref_jb_fungsional` (`REF_JB_FN_KODE`, `REF_JB_FN_NAMA`) VALUES
('DAITI01', 'DATA ANALYST'),
('KPIKA02', 'KEPALA INSTALASI KESEHATAN ANAK'),
('KPITI01', 'KEPALA INSTALASI TEKNNOLOGI INFORMASI'),
('PRIKA02', 'PERAWAT'),
('PRITI01', 'PROGRAMMER');

-- --------------------------------------------------------

--
-- Table structure for table `ref_level_kesulitan`
--

CREATE TABLE `ref_level_kesulitan` (
  `RLK_KODE` int(1) NOT NULL,
  `RLK_NAMA` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_level_kesulitan`
--

INSERT INTO `ref_level_kesulitan` (`RLK_KODE`, `RLK_NAMA`) VALUES
(1, 'MUDAH'),
(2, 'SEDANG'),
(3, 'SULIT');

-- --------------------------------------------------------

--
-- Table structure for table `trans_jabatan_pegawai`
--

CREATE TABLE `trans_jabatan_pegawai` (
  `TJB_KODE` int(11) NOT NULL,
  `MST_PEG_KODE` int(11) NOT NULL,
  `REF_JAB_KODE` varchar(11) NOT NULL,
  `TJB_IS_VERIF` int(1) NOT NULL,
  `TJP_TGL_AWAL` date DEFAULT NULL,
  `TJP_TGL_AKHIR` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jabatan_pegawai`
--

INSERT INTO `trans_jabatan_pegawai` (`TJB_KODE`, `MST_PEG_KODE`, `REF_JAB_KODE`, `TJB_IS_VERIF`, `TJP_TGL_AWAL`, `TJP_TGL_AKHIR`) VALUES
(1, 1, 'KPITI01', 0, NULL, NULL),
(2, 2, 'DAITI01', 0, NULL, NULL),
(3, 3, 'DAITI01', 1, NULL, NULL),
(4, 4, 'DAITI01', 0, NULL, NULL),
(10, 5, 'PRITI01', 0, NULL, NULL),
(11, 6, 'DAITI01', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trans_logbook`
--

CREATE TABLE `trans_logbook` (
  `TLB_ID` int(11) NOT NULL,
  `TLB_TANGGAL` datetime NOT NULL,
  `MST_PEGAWAI_MPG_KODE` int(11) NOT NULL,
  `TLB_NAMA_PEGAWAI` varchar(100) NOT NULL,
  `MST_UNIT_MSU_KODE` int(11) NOT NULL,
  `MST_KEG_LOGB_MKL_KODE` int(11) NOT NULL,
  `TLB_NAMA_KEGIATAN` varchar(200) NOT NULL,
  `TLB_KETERANGAN_KEGIATAN` varchar(200) NOT NULL,
  `TLB_IS_VERIF` int(1) NOT NULL DEFAULT '0',
  `TLB_TANGGAL_AKHIR` datetime NOT NULL,
  `TLB_QTY` int(3) NOT NULL,
  `TLB_SOURCE_DATA` int(1) NOT NULL,
  `TLB_OUTPUT` varchar(20) NOT NULL,
  `REF_LVL_KESLTN_RLK_KODE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_logbook`
--

INSERT INTO `trans_logbook` (`TLB_ID`, `TLB_TANGGAL`, `MST_PEGAWAI_MPG_KODE`, `TLB_NAMA_PEGAWAI`, `MST_UNIT_MSU_KODE`, `MST_KEG_LOGB_MKL_KODE`, `TLB_NAMA_KEGIATAN`, `TLB_KETERANGAN_KEGIATAN`, `TLB_IS_VERIF`, `TLB_TANGGAL_AKHIR`, `TLB_QTY`, `TLB_SOURCE_DATA`, `TLB_OUTPUT`, `REF_LVL_KESLTN_RLK_KODE`) VALUES
(1, '2019-12-16 17:19:00', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan Penelitian', 'Melakukan Penelitian', 0, '2019-12-17 00:00:00', 1, 1, 'selesai', 2),
(2, '2019-12-16 00:00:00', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'Kegiatan 3', 1, '2019-12-17 00:00:00', 1, 1, 'done', 2),
(8, '2019-12-30 10:12:03', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'a', 0, '2019-12-30 10:12:03', 1, 2, 'a', 1),
(12, '2019-12-30 16:45:54', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'g', 0, '2019-12-30 16:45:54', 1, 2, 'g', 1),
(13, '2020-01-01 15:24:04', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'sampel', 1, '2020-01-01 15:24:04', 1, 2, 'sampel', 2),
(14, '2020-01-01 17:03:31', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'lol', 1, '2020-01-22 17:03:31', 1, 2, 'lol', 1),
(15, '2020-01-02 20:01:55', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'cepet ta', 1, '2020-01-02 20:01:55', 1, 2, 'cepet ta', 1),
(16, '2020-01-04 06:32:17', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'testing iii', 1, '2020-01-04 06:32:17', 1, 2, 'testing yyy jj', 1),
(17, '2020-01-05 20:35:58', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'mm', 1, '2020-01-05 20:35:58', 1, 2, 'mm', 1),
(18, '2020-01-06 09:21:45', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'Kegiatan 3', 1, '2020-01-06 09:21:45', 1, 2, 'tidak selesai', 2),
(19, '2020-01-15 18:02:19', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'hari ini', 1, '2020-01-15 18:02:19', 1, 2, 'hari ini', 1),
(20, '2020-01-22 13:20:49', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'penelitian di vvvvv', 0, '2020-01-24 13:20:49', 1, 2, 'selesai', 2),
(21, '2020-01-22 13:24:42', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'kegatan hhh', 0, '2020-01-22 13:24:42', 1, 2, 'ss', 1),
(22, '2020-02-15 15:39:28', 2, 'SITI FATMAWATI', 1, 18, 'test666', 'test666', 1, '2020-02-15 15:39:28', 1, 2, 'selesai', 1),
(23, '2020-01-22 13:24:42', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'kegatan hhh', 0, '2020-01-22 13:24:42', 1, 2, 'ss', 1),
(24, '2020-02-15 21:11:55', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'kkkjkjk', 1, '2020-02-15 21:11:55', 1, 2, 'selesai', 1),
(25, '2020-02-15 21:12:21', 2, 'SITI FATMAWATI', 1, 18, 'test666', 'ksjkajkajsk', 0, '2020-02-15 21:12:21', 1, 2, 'selesai', 1),
(26, '2020-02-22 09:15:22', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'penelitian di amikom', 0, '2020-02-22 09:15:22', 1, 2, 'selesai', 2),
(27, '2020-03-14 11:53:08', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'JJKJKJK', 1, '2020-03-14 11:53:08', 1, 2, 'selesai', 1),
(28, '2020-03-15 10:02:26', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'klklk', 1, '2020-03-15 10:02:26', 1, 2, 'selesai', 2),
(29, '2020-03-15 11:43:39', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'klkk', 1, '2020-03-15 11:43:39', 1, 2, 'selesai', 1),
(30, '2020-04-01 10:23:08', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian untuk kemajuan Rumah Sakit', 1, '2020-04-01 10:23:08', 1, 2, 'selesai', 2),
(31, '2020-04-01 10:47:19', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'Kegiatan Pengembangan Sumber Daya Manusia', 1, '2020-04-16 10:46:59', 1, 2, 'selesai', 1),
(32, '2020-04-01 10:24:38', 2, 'SITI FATMAWATI', 1, 15, 'test1', 'Melakukan Pengembangan Aplikasi Penilaian Kinerja Karyawan', 1, '2020-04-01 10:24:38', 1, 2, 'selesai', 3),
(33, '2020-04-01 10:25:19', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian IOT', 1, '2020-04-03 10:25:19', 1, 2, 'selesai', 2),
(34, '2020-04-01 10:26:05', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian IOT', 1, '2020-04-03 10:26:05', 1, 2, 'selesai', 2),
(35, '2020-04-01 10:26:42', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian IOT', 1, '2020-04-01 10:26:42', 1, 2, 'selesai', 2),
(36, '2020-04-01 10:27:18', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'KKKKK', 0, '2020-04-03 10:27:18', 1, 2, 'selesai', 2),
(37, '2020-04-01 10:36:22', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian AI', 0, '2020-04-03 10:36:22', 1, 2, 'selesai', 2),
(38, '2020-04-01 10:37:56', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian AI', 0, '2020-04-03 10:37:56', 1, 2, 'selesai', 2),
(39, '2020-04-01 10:43:33', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'mm', 0, '2020-04-09 10:43:33', 1, 2, 'selesai', 2),
(40, '2020-04-01 10:55:34', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian AI Robotik', 0, '2020-04-03 10:55:34', 1, 2, 'selesai', 2),
(41, '2020-04-01 11:13:22', 2, 'SITI FATMAWATI', 1, 3, 'Melakukan penelitian', 'Penelitian ke 11', 1, '2020-04-03 11:13:22', 1, 2, 'selesai', 2),
(42, '2020-04-01 11:16:53', 2, 'SITI FATMAWATI', 1, 4, 'Kegiatan 3', 'nmnm', 1, '2020-04-01 11:16:53', 1, 2, 'ditunda', 2);

-- --------------------------------------------------------

--
-- Table structure for table `trans_unit_pegawai`
--

CREATE TABLE `trans_unit_pegawai` (
  `TUP_ID` int(11) NOT NULL,
  `MST_PEGAWAI_MPG_KODE` int(11) NOT NULL,
  `MST_UNIT_MSU_KODE` int(11) NOT NULL,
  `TUP_TGL_AWAL` timestamp NULL DEFAULT NULL,
  `TUP_TGL_AHIR` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_unit_pegawai`
--

INSERT INTO `trans_unit_pegawai` (`TUP_ID`, `MST_PEGAWAI_MPG_KODE`, `MST_UNIT_MSU_KODE`, `TUP_TGL_AWAL`, `TUP_TGL_AHIR`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 2, NULL, NULL),
(4, 4, 2, NULL, NULL),
(13, 5, 1, NULL, NULL),
(14, 6, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `def_jabatan_iki`
--
ALTER TABLE `def_jabatan_iki`
  ADD PRIMARY KEY (`DJI_KODE`),
  ADD KEY `REF_JAB_KODE` (`REF_JAB_KODE`),
  ADD KEY `MST_IKI_KODE` (`MST_IKI_KODE`) USING BTREE;

--
-- Indexes for table `def_jabatan_logbook`
--
ALTER TABLE `def_jabatan_logbook`
  ADD PRIMARY KEY (`DJL_KODE`),
  ADD KEY `MKL_KODE` (`MKL_KODE`),
  ADD KEY `REF_JAB_KODE` (`REF_JAB_KODE`);

--
-- Indexes for table `def_unit_iki`
--
ALTER TABLE `def_unit_iki`
  ADD PRIMARY KEY (`DUI_KODE`),
  ADD KEY `MST_UNIT_MSU_KODE` (`MST_UNIT_MSU_KODE`);

--
-- Indexes for table `def_unit_logbook`
--
ALTER TABLE `def_unit_logbook`
  ADD PRIMARY KEY (`DUL_KODE`),
  ADD KEY `MKL_KODE` (`MKL_KODE`),
  ADD KEY `MST_UNIT_MSU_KODE` (`MST_UNIT_MSU_KODE`);

--
-- Indexes for table `mst_iki`
--
ALTER TABLE `mst_iki`
  ADD PRIMARY KEY (`MST_IKI_KODE`);

--
-- Indexes for table `mst_kegiatan_logbook`
--
ALTER TABLE `mst_kegiatan_logbook`
  ADD PRIMARY KEY (`MKL_KODE`);

--
-- Indexes for table `mst_pegawai`
--
ALTER TABLE `mst_pegawai`
  ADD PRIMARY KEY (`MPG_KODE`),
  ADD KEY `REF_AGAMA_REG_KODE` (`REF_AGAMA_RAG_KODE`);

--
-- Indexes for table `mst_unit`
--
ALTER TABLE `mst_unit`
  ADD PRIMARY KEY (`MSU_KODE`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`ID_PENILAIAN`);

--
-- Indexes for table `ref_agama`
--
ALTER TABLE `ref_agama`
  ADD PRIMARY KEY (`RAG_KODE`);

--
-- Indexes for table `ref_jb_fungsional`
--
ALTER TABLE `ref_jb_fungsional`
  ADD PRIMARY KEY (`REF_JB_FN_KODE`);

--
-- Indexes for table `ref_level_kesulitan`
--
ALTER TABLE `ref_level_kesulitan`
  ADD PRIMARY KEY (`RLK_KODE`);

--
-- Indexes for table `trans_jabatan_pegawai`
--
ALTER TABLE `trans_jabatan_pegawai`
  ADD PRIMARY KEY (`TJB_KODE`),
  ADD KEY `REF_JAB_KODE` (`REF_JAB_KODE`),
  ADD KEY `MST_PEG_KODE` (`MST_PEG_KODE`);

--
-- Indexes for table `trans_logbook`
--
ALTER TABLE `trans_logbook`
  ADD PRIMARY KEY (`TLB_ID`),
  ADD KEY `MST_KEG_LOGB_MKL_KODE` (`MST_KEG_LOGB_MKL_KODE`),
  ADD KEY `REF_LVL_KESLTN_RLK_KODE` (`REF_LVL_KESLTN_RLK_KODE`),
  ADD KEY `MST_PEGAWAI_MPG_KODE` (`MST_PEGAWAI_MPG_KODE`);

--
-- Indexes for table `trans_unit_pegawai`
--
ALTER TABLE `trans_unit_pegawai`
  ADD PRIMARY KEY (`TUP_ID`),
  ADD KEY `MST_UNIT_MSU_KODE` (`MST_UNIT_MSU_KODE`),
  ADD KEY `MST_PEGAWAI_MPG_KODE` (`MST_PEGAWAI_MPG_KODE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `def_jabatan_iki`
--
ALTER TABLE `def_jabatan_iki`
  MODIFY `DJI_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `def_jabatan_logbook`
--
ALTER TABLE `def_jabatan_logbook`
  MODIFY `DJL_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `def_unit_iki`
--
ALTER TABLE `def_unit_iki`
  MODIFY `DUI_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `def_unit_logbook`
--
ALTER TABLE `def_unit_logbook`
  MODIFY `DUL_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mst_iki`
--
ALTER TABLE `mst_iki`
  MODIFY `MST_IKI_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `mst_kegiatan_logbook`
--
ALTER TABLE `mst_kegiatan_logbook`
  MODIFY `MKL_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `mst_unit`
--
ALTER TABLE `mst_unit`
  MODIFY `MSU_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `ID_PENILAIAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `ref_agama`
--
ALTER TABLE `ref_agama`
  MODIFY `RAG_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ref_level_kesulitan`
--
ALTER TABLE `ref_level_kesulitan`
  MODIFY `RLK_KODE` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trans_jabatan_pegawai`
--
ALTER TABLE `trans_jabatan_pegawai`
  MODIFY `TJB_KODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `trans_logbook`
--
ALTER TABLE `trans_logbook`
  MODIFY `TLB_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `trans_unit_pegawai`
--
ALTER TABLE `trans_unit_pegawai`
  MODIFY `TUP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `def_jabatan_iki`
--
ALTER TABLE `def_jabatan_iki`
  ADD CONSTRAINT `def_jabatan_iki_ibfk_2` FOREIGN KEY (`REF_JAB_KODE`) REFERENCES `ref_jb_fungsional` (`REF_JB_FN_KODE`);

--
-- Constraints for table `def_jabatan_logbook`
--
ALTER TABLE `def_jabatan_logbook`
  ADD CONSTRAINT `def_jabatan_logbook_ibfk_1` FOREIGN KEY (`MKL_KODE`) REFERENCES `mst_kegiatan_logbook` (`MKL_KODE`),
  ADD CONSTRAINT `def_jabatan_logbook_ibfk_2` FOREIGN KEY (`REF_JAB_KODE`) REFERENCES `ref_jb_fungsional` (`REF_JB_FN_KODE`);

--
-- Constraints for table `def_unit_iki`
--
ALTER TABLE `def_unit_iki`
  ADD CONSTRAINT `def_unit_iki_ibfk_2` FOREIGN KEY (`MST_UNIT_MSU_KODE`) REFERENCES `mst_unit` (`MSU_KODE`);

--
-- Constraints for table `def_unit_logbook`
--
ALTER TABLE `def_unit_logbook`
  ADD CONSTRAINT `def_unit_logbook_ibfk_1` FOREIGN KEY (`MKL_KODE`) REFERENCES `mst_kegiatan_logbook` (`MKL_KODE`),
  ADD CONSTRAINT `def_unit_logbook_ibfk_2` FOREIGN KEY (`MST_UNIT_MSU_KODE`) REFERENCES `mst_unit` (`MSU_KODE`);

--
-- Constraints for table `mst_pegawai`
--
ALTER TABLE `mst_pegawai`
  ADD CONSTRAINT `mst_pegawai_ibfk_1` FOREIGN KEY (`REF_AGAMA_RAG_KODE`) REFERENCES `ref_agama` (`RAG_KODE`);

--
-- Constraints for table `trans_jabatan_pegawai`
--
ALTER TABLE `trans_jabatan_pegawai`
  ADD CONSTRAINT `trans_jabatan_pegawai_ibfk_2` FOREIGN KEY (`REF_JAB_KODE`) REFERENCES `ref_jb_fungsional` (`REF_JB_FN_KODE`),
  ADD CONSTRAINT `trans_jabatan_pegawai_ibfk_3` FOREIGN KEY (`MST_PEG_KODE`) REFERENCES `mst_pegawai` (`MPG_KODE`);

--
-- Constraints for table `trans_logbook`
--
ALTER TABLE `trans_logbook`
  ADD CONSTRAINT `trans_logbook_ibfk_1` FOREIGN KEY (`MST_KEG_LOGB_MKL_KODE`) REFERENCES `mst_kegiatan_logbook` (`MKL_KODE`),
  ADD CONSTRAINT `trans_logbook_ibfk_3` FOREIGN KEY (`REF_LVL_KESLTN_RLK_KODE`) REFERENCES `ref_level_kesulitan` (`RLK_KODE`),
  ADD CONSTRAINT `trans_logbook_ibfk_4` FOREIGN KEY (`MST_PEGAWAI_MPG_KODE`) REFERENCES `mst_pegawai` (`MPG_KODE`);

--
-- Constraints for table `trans_unit_pegawai`
--
ALTER TABLE `trans_unit_pegawai`
  ADD CONSTRAINT `trans_unit_pegawai_ibfk_2` FOREIGN KEY (`MST_UNIT_MSU_KODE`) REFERENCES `mst_unit` (`MSU_KODE`),
  ADD CONSTRAINT `trans_unit_pegawai_ibfk_3` FOREIGN KEY (`MST_PEGAWAI_MPG_KODE`) REFERENCES `mst_pegawai` (`MPG_KODE`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
